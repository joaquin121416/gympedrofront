# GymProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

##Capturas

![alt text](https://lh3.googleusercontent.com/pw/ACtC-3clj1dk__y8rtlLmy8gaw2cPCAHved_dJSgvlrhwpru-V3ILlERCzllQavhrqIjNy5GuPihKOhGM7ENABRFYoeFPlWklHWG40B-AmPCS_iKyiE8SJAxOby6aQO06OfB-gjvm_cpptm-ukR6Cb7GvaCP=w1280-h566-no?authuser=0)


![alt text](https://lh3.googleusercontent.com/pw/ACtC-3evWTFRkz04bmjuFr3AumV0bSxpLBOzbLqjUHjnzCTW7mKnqf9dOIX7twhMNOQzyuxU1W4NhnNuoAgyV_wQe27lOqZ1X79V6P1cQ_DHpMKm7eVmBYDe4W54MAFoqx6PEIxm25JNywewp5G-n7N3ydUk=w1280-h546-no?authuser=0)


![alt text](https://lh3.googleusercontent.com/pw/ACtC-3fR-xElve0PS_Hrqgrs3Zh2eF3Bwt5mrNzvm42dYYwEoJ7azn8cmXCugtDvq298drLfMO9gGwJzqINWHABNQ1ZwqVwOeGNQ2sdtdOt42Wd9ehQ6l72moW4sYiLw-2_928zuovKD3PHwvk6g1vXnaXpY=w1280-h508-no?authuser=0)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
